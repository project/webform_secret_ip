CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Information
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Current Maintainer: lexsoft <https://www.drupal.org/u/lexsoft>

Webform Hide IP is a very simple, lightweight module that gives the creator
of a webform the option to anonymize the last 2 digits of IP Address for users
who submitted the webform.

INFORMATION
-----------

This module adds Anonymize IP Settings in the webform form settings to
Anonymize last 2 digitis of IP address.

The module is completely compatible with webform. It only anonymizes the user
IP when the submission are saved.

REQUIREMENTS
------------

This module requires the following module:
 * Webform (https://www.drupal.org/project/webform).

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
     https://www.drupal.org/docs/7/extend/installing-modules
     for further information.

CONFIGURATION
-------------

 * Setup your webforms by editing a webform node and going to the Webform tab,
     and then clicking Form settings, scrolling down to Anonymize IP Settings.
